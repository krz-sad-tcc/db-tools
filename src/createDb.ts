import { Database } from 'arangojs';
import { withErrorHandling } from './withErrorHandling';

export const createDb = async (
  connection: Database,
  dbName: string,
  dbUser: string,
  dbPass: string,
) => {
  return withErrorHandling(async () => {
    let db;
    db = await connection.createDatabase(dbName, {
      users: [{ username: dbUser }],
    });
    db.database(dbName);
    db.useBasicAuth(dbUser, dbPass);

    return db;
  });
};
