import { Database } from 'arangojs';
import { withErrorHandling } from './withErrorHandling';

export const createGraph = async (
  db: Database,
  graphName: string,
  edgesCollection: string,
  verticesCollection: string,
) => {
  await withErrorHandling(async () => {
    await db.createGraph(graphName, [
      {
        collection: edgesCollection,
        from: verticesCollection,
        to: verticesCollection,
      },
    ]);
  });
};
