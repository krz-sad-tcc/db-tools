import { Database } from 'arangojs';
import { withErrorHandling } from './withErrorHandling';

export const dropDb = async (connection: Database, dbName: string) => {
  await withErrorHandling(async () => {
    const dbs = await connection.databases();

    if (dbs.map((d) => d.name).includes(dbName)) {
      await connection.dropDatabase(dbName);
    }
  });
};
