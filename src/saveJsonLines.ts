import { Database } from 'arangojs';
import { COLL_LEADS_TO, COLL_POSITIONS } from './constants';
import * as events from 'node:events';
import * as fs from 'node:fs';
import * as readline from 'node:readline';

export const saveJsonLines = async (db: Database, dataDir: string) => {
  const leadsToCollection = db.collection(COLL_LEADS_TO);
  const positionsCollection = db.collection(COLL_POSITIONS);
  const leadsToLines: any[] = [];
  const positionsLines: any[] = [];

  try {
    const positionsReader = readline.createInterface({
      input: fs.createReadStream(`${dataDir}/positions.jsonl`),
      crlfDelay: Infinity,
    });

    positionsReader.on('line', (line: string) => {
      positionsLines.push(JSON.parse(line));
    });

    await events.once(positionsReader, 'close');
    await positionsCollection.saveAll(positionsLines);
  } catch (err) {
    console.error(err);
  }

  try {
    const leadsToReader = readline.createInterface({
      input: fs.createReadStream(`${dataDir}/leads_to.jsonl`),
      crlfDelay: Infinity,
    });

    leadsToReader.on('line', (line: string) => {
      leadsToLines.push(JSON.parse(line));
    });

    await events.once(leadsToReader, 'close');
    await leadsToCollection.saveAll(leadsToLines);
  } catch (err) {
    console.error(err);
  }
};
