import { ArangoError } from 'arangojs/error';

const enhancedArangoError = (error: ArangoError) => {
  console.error(error.stack);
  return new Error(error.message);
};

/**
 * Wraps access to arango and handles eventual errors displaying a more
 * comprehensive stack trace.
 */
export const withErrorHandling = async <T>(cb: () => T) => {
  try {
    return cb();
  } catch (error) {
    if (error instanceof ArangoError) {
      throw enhancedArangoError(error);
    } else {
      throw error;
    }
  }
};
