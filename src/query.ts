import { Database } from 'arangojs';
import { AqlQuery } from 'arangojs/aql';
import { ArrayCursor } from 'arangojs/cursor';
import { withErrorHandling } from './withErrorHandling';

type ArangoCursor<T> = ArrayCursor<T>;

export const query = async <T>(
  db: Database,
  aqlQuery: AqlQuery,
): Promise<T[]> => {
  return withErrorHandling(async () => {
    const result = (await db.query(aqlQuery)) as ArangoCursor<T>;
    return result.all();
  });
};
