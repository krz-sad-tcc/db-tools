import { Database } from 'arangojs';
import { env } from './env';

export const connection = new Database({
  url: env.DB_HOST,
  auth: { username: env.DB_USER, password: env.DB_PASS },
  precaptureStackTraces: true,
});
