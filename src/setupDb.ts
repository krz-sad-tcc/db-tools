import { Database } from 'arangojs';
import { COLL_LEADS_TO, COLL_POSITIONS, GRAPH } from './constants';
import { createDb } from './createDb';
import { createGraph } from './createGraph';
import { dropDb } from './dropDb';

export const setupDb = async (
  connection: Database,
  dbName: string,
  dbUser: string,
  dbPass: string,
) => {
  console.log(`Setting up database "${dbName}"...`);

  await dropDb(connection, dbName);
  console.log(`* Database "${dbName}" dropped.`);

  const db = await createDb(connection, dbName, dbUser, dbPass);
  console.log(`* Database "${dbName}" created.`);

  await db.createCollection(COLL_POSITIONS);
  console.log(`* Collection "${COLL_POSITIONS}" created.`);

  await db.createEdgeCollection(COLL_LEADS_TO);
  console.log(`* Collection "${COLL_LEADS_TO}" created.`);

  await createGraph(db, GRAPH, COLL_LEADS_TO, COLL_POSITIONS);
  console.log(`* Graph "${GRAPH}" created.`);
};
