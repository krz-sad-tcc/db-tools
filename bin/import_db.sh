#!/bin/bash

if test -f .env; then
  source .env
fi

DATA_DIR=$1

echo '
  const connect = async () => {
    const { connection, setupDb } = require("@krz-sad-tcc/db-tools");
    await setupDb(connection, process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS);
  };
  connect();
' | node

arangoimport \
  --server.database "$DB_NAME" \
  --server.username "$DB_USER" \
  --server.password "$DB_PASS" \
  --file "$DATA_DIR/positions.jsonl" \
  --collection "positions" \
  --type "jsonl" \
  --create-collection \
  --overwrite

arangoimport \
  --server.database "$DB_NAME" \
  --server.username "$DB_USER" \
  --server.password "$DB_PASS" \
  --file "$DATA_DIR/leads_to.jsonl" \
  --collection "leads_to" \
  --type "jsonl" \
  --create-collection \
  --create-collection-type "edge" \
  --from-collection-prefix "positions" \
  --to-collection-prefix "positions" \
  --overwrite
