#!/bin/bash

if test -f .env; then
  source .env
fi

DATA_DIR=$1

arangoexport \
  --server.database "$DB_NAME" \
  --server.username "$DB_USER" \
  --server.password "$DB_PASS" \
  --collection "leads_to" \
  --collection "positions" \
  --type jsonl \
  --output-directory "./$DATA_DIR" \
  --overwrite

rm -f data/ENCRYPTION
